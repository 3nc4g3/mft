#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import math
import numpy as np
import matplotlib.pyplot as plt
import glob
import os
import time
import cStringIO
import operator

# page header
	# 0x100	256
	# 		512
	# 		768
	# 		1024

# working record

# redo : 작업 후 데이터
# undo : 작업 전 데이터

# def print_logfile():
# 	global current_lsn
# 	global filename
# 	logfile_list = glob.glob('C:\Temp\logfiles\*')
# 	for file in logfile_list:
# 		filename = file
# 		f = open(file, 'r')
# 		# 1 page == 4096 byte == 0x1000
# 		page_size = 4096
# 		offset = 0
# 		restart_area = 0
# 		buffer_page_area = 0
# 		normal_page_area = 0

# 		current_lsn = ""

# 		while True:
# 			buf16 = f.read(16)
# 			buf16_length = len(buf16)
# 			if buf16_length == 0:
# 				break

# 			# 출력 부
# 			output = "%08X:  " % (offset) # Offset(번지)을, 출력 버퍼에 쓰기

# 			for i in range(buf16_length): # 헥사 부분의 헥사 값 16개 출력 (8개씩 2부분으로)
# 				if (i == 8): output += " " # 8개씩 분리
# 				output += "%02X " % (ord(buf16[i])) # 헥사 값 출력

# 			for i in range( ((16 - buf16_length) * 3) + 1 ): # 한 줄이 16 바이트가 되지 않을 때, 헥사 부분과 문자 부분 사이에 공백들 삽입
# 				output += " "
# 			if (buf16_length < 9):
# 				output += " " # 한줄이 9바이트보다 적을 때는 한칸 더 삽입

# 			for i in range(buf16_length): # 문자 구역 출력
# 				if (ord(buf16[i]) >= 0x20 and ord(buf16[i]) <= 0x7E): # 특수 문자 아니면 그대로 출력
# 				  output += buf16[i]
# 				else: output += "." # 특수문자, 그래픽문자 등은 마침표로 출력

# 			offset += 16 # 번지 값을 16 증가
# 			print output # 1행 분량의 헥사 덤프 문자열이 든 버퍼를 출력
			

# 		if (offset == 0):
#   			print "%08X:  " % (offset) # 0바이트 파일일 경우 처리

# 		f.close()
# 		break


class Logfile_data:
	def __init__(self, page_number, offset, lsn=None, crn_lsn=None, prv_lsn=None, undo_lsn=None, length=None, redo_op=None, undo_op=None, redo_offset=None, redo_length=None, undo_offset=None, undo_length=None, redo_data=None, undo_data=None, record_offset=None, attr_offset=None, target_lcn=None):
		self.page_number = page_number
		self.offset = offset
		self.lsn = lsn
		self.crn_lsn = crn_lsn
		self.prv_lsn = prv_lsn
		self.undo_lsn = undo_lsn
		self.length = length
		self.redo_op = redo_op
		self.undo_op = undo_op
		self.redo_offset = redo_offset
		self.redo_length = redo_length
		self.undo_offset = undo_offset
		self.undo_length = undo_length
		self.redo_data = redo_data
		self.undo_data = undo_data
		self.record_offset = record_offset
		self.attr_offset = attr_offset
		self.target_lcn = target_lcn


class Data_list:
	def __init__(self, data_list=[]):
		self.data_list = data_list

	def add_data(self, data):
		self.data_list.append(data)

	def delete_data(self):
		self.data_list = []

	def get_data_list(self):
		return self.data_list

	def get_data_crn_lsn(lsn):
		for data in data_list:
			if data.crn_lsn == lsn:
				return data
			else:
				continue

	def get_data_prv_lsn(lsn):
		for data in data_list:
			if data.prv_lsn == lsn:
				return data
			else:
				continue


total_data_list = Data_list()
total_rank_1_gram_dict = {}
total_rank_2_gram_dict = {}
total_rank_3_gram_dict = {}
total_rank_4_gram_dict = {}
mal_count_dict = {}
current_lsn = ""
start = 0
cdm_list =[]
cdu_list =[]
cmd_list =[]
cud_list =[]
dcm_list =[]
dcu_list =[]
mmm_list =[]
mum_list =[]
file_name_list = []
ransom_score_list = []


def parse_logfile():
	global total_data_list
	global current_lsn
	global start
	global total_rank_1_gram_dict
	global total_rank_2_gram_dict
	global total_rank_3_gram_dict
	global total_rank_4_gram_dict

	op_list = [('0e00', '0f00'), ('0200', '0000'), ('0f00', '0e00'), ('0300', '0200'), ('0700', '0700'), ('0900', '0900'), ('0600', '0500'), ('0500', '0600')]

	file_numb = 0
	logfile_list = glob.glob('logfiles/*')
	file_count = len(logfile_list)
	for file in logfile_list:
		start = time.time()
		f = open(file, 'r')
		file_name = file[file.rfind('/') + 1 :]
		print "***************************************************************"
		print "target logfile : " + file_name
		print "***************************************************************"
		# 1 page == 4096 byte == 0x1000
		page_size = 4096
		offset = 0
		page_number = 0

		total_data_list.delete_data()
		while True:
			
			f.seek(offset)
			page_str = f.read(page_size)
			page_length = len(page_str)
			if page_length < 4096:
				break

			f.seek(offset)
			# parse all data

			# restart area
			if offset == 0:
				f.seek(48, 1)
				current_lsn = f.read(8).encode("hex")
				offset += 4096
				page_number += 1
				continue
			elif offset == 4096 * 1:
				offset += 4096
				page_number += 1
				continue
			# buffer area
			elif offset == 4096 * 2:
				offset += 4096
				page_number += 1
				continue
			elif offset == 4096 * 3:
				offset += 4096
				page_number += 1
				continue
			else:
				pass
			
			# single_page = f.read(4096).encode("hex")
			# sp = cStringIO.StringIO(single_page)
			# for i in range (0, 4096):
			# 	print sp.read(1),
			# 	if i % 32 == 31:
			# 		print ''
			# 	elif i % 8 == 7:
			# 		print ' ',
			# f.seek(-4096, 1)

			# skip page header
			f.seek(64, 1)
			offset += 64

			# record parsing
			while True:

				# print offset # first 0x2040 == 8256
				# record header parsing
				# record header length == 88 bytes (0x58)
				lsn = f.read(8).encode("hex")
				prv_lsn = f.read(8).encode("hex")
				undo_lsn = f.read(8).encode("hex")
				record_length = ''
				for i in range(0, 4):
					record_length = f.read(1).encode('hex') + record_length
				# redo offset + length -> record total length

				f.seek(4, 1)

				record_type = ''
				for i in range(0, 4):
					record_type = f.read(1).encode('hex') + record_type

				f.seek(4, 1)

				flag = ''
				for i in range(0, 2):
					flag = f.read(1).encode('hex') + flag

				f.seek(6, 1)

				redo_op = f.read(2).encode("hex")
				undo_op = f.read(2).encode("hex")
				
				redo_offset = ''
				for i in range(0, 2):
					redo_offset = f.read(1).encode('hex') + redo_offset

				redo_length = ''
				for i in range(0, 2):
					redo_length = f.read(1).encode('hex') + redo_length

				undo_offset = ''
				for i in range(0, 2):
					undo_offset = f.read(1).encode('hex') + undo_offset

				undo_length = ''
				for i in range(0, 2):
					undo_length = f.read(1).encode('hex') + undo_length
				
				f.seek(2, 1)

				lcn_to_follow = ''
				for i in range(0, 2):
					lcn_to_follow = f.read(1).encode('hex') + lcn_to_follow

				record_offset = ''
				for i in range(0, 2):
					record_offset = f.read(1).encode('hex') + record_offset

				# if record_offset == '0000':
				# 	print "not MFT record"
	
				attr_offset = ''
				for i in range(0, 2):
					attr_offset = f.read(1).encode('hex') + attr_offset

				f.seek(12, 1)

				# target LCN
				target_lcn = f.read(4).encode('hex')

				# redo data
				f.seek(offset)
				f.seek(48, 1)

				# redo op 위치부터
				if redo_offset == '':
					break
				f.seek(int(redo_offset, base=16), 1)
				if int(redo_length,base=16) < 4096:
					redo_data = f.read(int(redo_length, base=16)).encode("hex")
				else:
					redo_data = ''

				# undo data
				f.seek(offset)
				f.seek(48, 1)
				# print "undo offset : " + undo_offset
				
				# redo op 위치부터
				if undo_offset == '':
					break
				f.seek(int(undo_offset, base=16), 1)
				# print "undo length : " + undo_length
				if int(undo_length, base=16) < 4096:
					undo_data = f.read(int(undo_length, base=16)).encode("hex")
				else:
					undo_data = ''

				if (redo_op, undo_op) in op_list:

					logfile_data = Logfile_data(page_number, offset, lsn, lsn, prv_lsn, undo_lsn, record_length, redo_op, undo_op, redo_offset, redo_length, undo_offset, undo_length, redo_data, undo_data, record_offset, attr_offset, target_lcn)
					total_data_list.add_data(logfile_data)
					
					# if flag == '0001':
					# 	print "*************************************"
					# 	print "page number : " + hex(page_number)
					# 	print "offset : "  + hex(offset)
					# 	print "lsn : " + lsn
					# 	print "record length : " + str(int(record_length, base=16))
					# 	print "redo op : " + redo_op
					# 	print "undo op : " + undo_op
					# 	print "redo_offset : " + redo_offset
					# 	print "redo_length : " + str(int(redo_length, base=16))
					# 	print "undo_offset : " + undo_offset
					# 	print "undo_length : " + str(int(undo_length, base=16))

					# 	print "redo_data"
					# 	re = cStringIO.StringIO(redo_data)
					# 	for i in range (0, len(redo_data)):
					# 		print re.read(1),
					# 		if i % 32 == 31:
					# 			print ''
					# 		elif i % 8 == 7:
					# 			print ' ',

					# 	print ''
					# 	print "undo_data"
					# 	un = cStringIO.StringIO(undo_data)
					# 	for i in range (0, len(undo_data)):
					# 		print un.read(1),
					# 		if i % 32 == 31:
					# 			print ''
					# 		elif i % 8 == 7:
					# 			print ' ',
					# 	print ''
					# 	print "*************************************"

				and_result = 0
				if offset + 48 + int(record_length, base=16) > 0xFFFFFFFF:
					and_result = int(record_length, base=16) & (offset + 48)
					temp_offset = and_result
				else:
					temp_offset = offset + 48 + int(record_length, base=16)

				# page overflow
				if temp_offset > (page_number + 1) * 4096:
					temp_offset = (page_number + 1) * 4096
					offset = temp_offset
					page_number += 1
					f.seek(offset)
					# print "type 1 offset : " + hex(offset)
					break
				# page ended
				elif temp_offset == (page_number + 1) * 4096:
					# print '%d page ended!' % page_number
					offset = temp_offset
					page_number += 1
					f.seek(offset)
					# print "type 2 offset : " + hex(offset)
					break
				else:
					offset = temp_offset
					f.seek(offset)
					# print "record_length : " + record_length
					# print "type 3 offset : " + hex(offset)
					# break
					continue
			# if page_number > 10:
			# 	break
		
		file_numb += 1
		print_logfile_data(total_data_list, file_name)
		f.close()
		# break
		# if file_numb == 1:
		# 	break

	# average rank
	# res_file = open("result/" + ".txt", "w")
	# for key, value in total_rank_1_gram_dict.items():
	# 	total_rank_1_gram_dict[key] = int(total_rank_1_gram_dict[key]) / int(count)
	# for key, value in total_rank_2_gram_dict.items():
	# 	total_rank_2_gram_dict[key] = int(total_rank_2_gram_dict[key]) / int(count)
	# for key, value in total_rank_3_gram_dict.items():
	# 	total_rank_3_gram_dict[key] = int(total_rank_3_gram_dict[key]) / int(count)
	# for key, value in total_rank_4_gram_dict.items():
	# 	total_rank_4_gram_dict[key] = int(total_rank_4_gram_dict[key]) / int(count)

	# print total_rank_1_gram_dict
	# print total_rank_2_gram_dict
	# print total_rank_3_gram_dict
	# print total_rank_4_gram_dict

		
def print_logfile_data(data_list, file_name):
	global total_data_list
	global start
	global mal_count_dict

	create = 0
	delete = 0
	update = 0
	rename = 0

	# toggle
	ct = False
	dt = False
	ut = False
	rt = False

	# crn lsn
	ct_crn = ''
	dt_crn = ''
	ut_crn = ''
	rt_crn = ''

	# target dt undo data
	dt_target_undo_data = ''
	rt_prev_redo_data = ''

	logfile_record_string = ''
	rename_dict = {}

	result_file = open("result/" + file_name + ".txt", "w")
	for data in total_data_list.get_data_list():
		if data.redo_op == '0e00' and data.undo_op == '0f00': # creation trigger
			ct = True
			ct_crn = data.lsn
			continue
		elif data.redo_op == '0200' and data.undo_op == '0000': # creation check
			
			if ct == True and ct_crn == data.prv_lsn: # create
				ct = False
				ct_crn = ''
				
				# result_file.write("page numb : " + str(hex(data.page_number)) + ", " + str(data.redo_op) + " / " + str(data.undo_op) + " => create operation\n")
				# result_file.write("redo offset : " + str(int(data.redo_offset, base=16)) + ", length : " + str(int(data.redo_length, base=16)) + ", undo offset : " + str(int(data.undo_offset, base=16)) + ", length : " + str(int(data.undo_length, base=16)) + "\n")
				logfile_record_string += 'C'
				create += 1
				# parse_redo_create_data(data.redo_data, data.offset)
			elif ct == True and ct_crn != data.prv_lsn: # no
				continue
			else: # no
				continue

		elif data.redo_op == '0f00' and data.undo_op == '0e00': # delete trigger
			dt_target_undo_data = data.undo_data
			dt = True
			dt_crn = data.lsn
			continue
		elif data.redo_op == '0300' and data.undo_op == '0200': # deletion check
			if dt == True and dt_crn == data.prv_lsn: # delete
				dt = False
				dt_crn = ''
				# result_file.write("page numb : " + str(hex(data.page_number)) + ", " + str(data.redo_op) + " / " + str(data.undo_op) + " => delete operation\n")
				# result_file.write("redo offset : " + str(int(data.redo_offset, base=16)) + ", length : " + str(int(data.redo_length, base=16)) + ", undo offset : " + str(int(data.undo_offset, base=16)) + ", length : " + str(int(data.undo_length, base=16)) + "\n")
				delete += 1
				logfile_record_string += 'D'
				# parse_undo_delete_data(dt_target_undo_data, data.offset)
				dt_target_undo_data = ''
			elif dt == True and dt_crn != data.prv_lsn: # no
				continue
			else: # no
				continue
			
		elif data.redo_op == '0700' and int(data.attr_offset, base=16) >= 0x18: # update
			# result_file.write("page numb : " + str(hex(data.page_number)) + ", " + str(data.redo_op) + " / " + str(data.undo_op) + " => update operation\n")
			# result_file.write("redo offset : " + str(int(data.redo_offset, base=16)) + ", length : " + str(int(data.redo_length, base=16)) + ", undo offset : " + str(int(data.undo_offset, base=16)) + ", length : " + str(int(data.undo_length, base=16)) + "\n")
			update += 1
			logfile_record_string += 'U'
			# parse_redo_update_data(data.redo_data, data.offset)
			# parse_undo_update_data(data.undo_data, data.offset)
			
		elif data.redo_op == '0900' and int(data.attr_offset, base=16) == 0x40: # update
			# result_file.write("page numb : " + str(hex(data.page_number)) + ", " + str(data.redo_op) + " / " + str(data.undo_op) + " => update operation\n")
			# result_file.write("redo offset : " + str(int(data.redo_offset, base=16)) + ", length : " + str(int(data.redo_length, base=16)) + ", undo offset : " + str(int(data.undo_offset, base=16)) + ", length : " + str(int(data.undo_length, base=16)) + "\n")
			update += 1
			logfile_record_string += 'U'
			# parse_redo_update_data(data.redo_data, data.offset)
			# parse_undo_update_data(data.undo_data, data.offset)

		elif data.redo_op == '0600' and data.undo_op == '0500': # rename trigger
			rt = True
			rt_crn = data.lsn
			# rt_prev_redo_data = data.redo_data
			if data.target_lcn in rename_dict:
				# parse_redo_prev_rename_data(data.redo_data)
				# parse_redo_crn_rename_data(rename_dict.get(data.target_lcn))
				del rename_dict[data.target_lcn]
				logfile_record_string += 'M'
				rename += 1
			else:
				rename_dict[data.target_lcn] = data.redo_data
			# if data.target_lcn in rename_dict:
			# else:
			continue
		elif data.redo_op == '0500' and data.undo_op == '0600': # rename check
			if rt == True and rt_crn == data.prv_lsn:
				rt = False
				rt_crn = ''
				# result_file.write("page numb : " + str(hex(data.page_number)) + ", " + str(data.redo_op) + " / " + str(data.undo_op) + " => rename operation\n")
				# result_file.write("redo offset : " + str(int(data.redo_offset, base=16)) + ", length : " + str(int(data.redo_length, base=16)) + ", undo offset : " + str(int(data.undo_offset, base=16)) + ", length : " + str(int(data.undo_length, base=16)) + "\n")
				if data.target_lcn in rename_dict:
					# parse_redo_prev_rename_data(rename_dict.get(data.target_lcn))
					# parse_redo_crn_rename_data(data.redo_data)
					del rename_dict[data.target_lcn]
					logfile_record_string += 'M'
					rename += 1
				else:
					rename_dict[data.target_lcn] = data.redo_data
				# parse_redo_prev_rename_data(rt_prev_redo_data)
				# parse_redo_crn_rename_data(data.redo_data)
				# rt_prev_redo_data = ''
			elif rt == True and rt_crn != data.prv_lsn: # no
				continue
			else: # no
				continue
		else:
			continue

	# print logfile_record_string
	# mal_count = 0
	# mal_count = rank_dict(logfile_record_string)
	# mal_count_dict[file_name] = mal_count
	mal_count = ''
	mal_count = rank_dict(logfile_record_string, file_name)
	mal_count_dict[file_name] = mal_count

	print "create : %d" % create
	print "delete : %d" % delete
	print "update : %d" % update
	print "rename : %d" % rename
	print "total : %d" % (create + delete + update + rename)
	result_file.write(logfile_record_string + "\n")
	result_file.write("create : %d\n" % create)
	result_file.write("delete : %d\n" % delete)
	result_file.write("update : %d\n" % update)
	result_file.write("rename : %d\n" % rename)
	result_file.write("total : %d\n" % (create + delete + update + rename))

	result_file.write("mal count\n\n" + mal_count)

	end = time.time() - start
	print file_name + " elasped time : " + str(end)
	result_file.write("\n\n" + file_name + " elasped time : " + str(end) + "\n")
	result_file.close()


def rank_dict(logfile_record_string, file_name):

	global total_rank_1_gram_dict
	global total_rank_2_gram_dict
	global total_rank_3_gram_dict
	global total_rank_4_gram_dict
	global ransom_score_list
	global file_name_list

	# print logfile_record_string
	logfile_record_length = len(logfile_record_string)

	rank_1_gram_dict = {}
	rank_2_gram_dict = {}
	rank_3_gram_dict = {}
	rank_4_gram_dict = {}

	record_string = cStringIO.StringIO(logfile_record_string)
	count = 0

	# for i in range(1,5):
	# 	record_string.seek(0)
	# 	while True:
	# 		pattern = record_string.read(i)

	# 		if i == 1:
	# 			if pattern in rank_1_gram_dict:
	# 				rank_1_gram_dict[pattern] = rank_1_gram_dict[pattern] + 1
	# 			else:
	# 				rank_1_gram_dict[pattern] = 1
	# 		elif i == 2:
	# 			if pattern in rank_2_gram_dict:
	# 				rank_2_gram_dict[pattern] = rank_2_gram_dict[pattern] + 1
	# 			else:
	# 				rank_2_gram_dict[pattern] = 1
	# 		elif i == 3:
	# 			if pattern in rank_3_gram_dict:
	# 				rank_3_gram_dict[pattern] = rank_3_gram_dict[pattern] + 1
	# 			else:
	# 				rank_3_gram_dict[pattern] = 1
	# 		elif i == 4:
	# 			if pattern in rank_4_gram_dict:
	# 				rank_4_gram_dict[pattern] = rank_4_gram_dict[pattern] + 1
	# 			else:
	# 				rank_4_gram_dict[pattern] = 1
	# 		else:
	# 			break

	# 		record_string.seek(-(i-1), 1)
	# 		if int(count + i) >= int(logfile_record_length):
	# 			count = 0
	# 			break
	# 		else:
	# 			count += 1
	# 			continue
	
	cdm = 0
	cdu = 0
	cmd = 0
	cud = 0
	dcm = 0
	dcu = 0
	mmm = 0
	mum = 0

	nhs_list = []

	hit = 0

	malstring = ''

	ransom_score = 0

	cud_weight = 10
	dcu_weight = 5
	mum_weight = 1
	cdm_weight = 1
	dcm_weight = 1
	cmd_weight = 1
	mmm_weight = 1
	cdu_weight = 1

	while True:
		f_event = record_string.read(1)
		count += 1
		
		if f_event == 'C':
			# dc
			if 'd' in nhs_list:
				nhs_list[nhs_list.index('d')] = 'dc'
			# c
			else:
				if len(nhs_list) > 10:
					del nhs_list[0]
					nhs_list.append('c')
				else:
					nhs_list.append('c')
		
		elif f_event == 'D':
			# cmd
			if 'cm' in nhs_list:
				del nhs_list[nhs_list.index('cm')]
				hit += 1
				cmd += 1
				malstring += 'CMD'
				ransom_score += cmd_weight
			# cud
			elif 'cu' in nhs_list:
				del nhs_list[nhs_list.index('cu')]
				hit += 1
				cud += 1
				malstring += 'CUD'
				ransom_score += cud_weight
			# cd
			elif 'c' in nhs_list:
				nhs_list[nhs_list.index('c')] = 'cd'
			# d
			else:
				if len(nhs_list) > 10:
					del nhs_list[0]
					nhs_list.append('d')
				else:
					nhs_list.append('d')

		elif f_event == 'M':
			# cdm
			if 'cd' in nhs_list:
				del nhs_list[nhs_list.index('cd')]				
				cdm += 1
				hit += 1
				malstring += 'CDM'
				ransom_score += cdm_weight
			# dcm 
			elif 'dc' in nhs_list:
				del nhs_list[nhs_list.index('dc')]
				dcm += 1
				hit += 1
				malstring += 'DCM'
				ransom_score += dcm_weight
			# mum
			elif 'mu' in nhs_list:
				del nhs_list[nhs_list.index('mu')]
				mum += 1
				hit += 1
				malstring += 'MUM'
				ransom_score += mum_weight
			# mmm
			elif 'mm' in nhs_list:
				del nhs_list[nhs_list.index('mm')]				
				mmm += 1
				hit += 1
				malstring += 'MMM'
				ransom_score += mmm_weight
			# mm
			elif 'm' in nhs_list:
				nhs_list[nhs_list.index('m')] = 'mm'
			# cm
			elif 'c' in nhs_list:
				nhs_list[nhs_list.index('c')] = 'cm'
			# m
			else:
				if len(nhs_list) > 10:
					del nhs_list[0]
					nhs_list.append('m')
				else:
					nhs_list.append('m')

		elif f_event == 'U':
			# cdu
			if 'cd' in nhs_list:
				del nhs_list[nhs_list.index('cd')]
				cdu += 1
				hit += 1
				malstring += 'CDU'
				ransom_score += cdu_weight
			# dcu
			elif 'dc' in nhs_list:
				del nhs_list[nhs_list.index('dc')]
				dcu += 1
				hit += 1
				malstring += 'DCU'
				ransom_score += dcu_weight
			# cu
			elif 'c' in nhs_list:
				nhs_list[nhs_list.index('c')] = 'cu'
			# mu
			elif 'm' in nhs_list:
				nhs_list[nhs_list.index('m')] = 'mu'
			# u (no)
			else:
				pass
		else:
			break

		# if f_event == 'C':
		# 	# dc
		# 	if d_init_stack_count > 0:
		# 		dc_count += 1
		# 	# c
		# 	else:
		# 		c_init_stack_count += 1
		# elif f_event == 'D':
		# 	# cmd
		# 	if cm_count > 0:
		# 		cm_count -= 1
		# 		cmd += 1
		# 		hit += 1
		# 		malstring += 'CMD'
		# 		ransom_score += cmd_weight
		# 	# cud
		# 	elif cu_count > 0:
		# 		cu_count -= 1
		# 		cud += 1
		# 		hit += 1
		# 		malstring += 'CUD'
		# 		ransom_score += cud_weight
		# 	# cd
		# 	elif c_init_stack_count > 0:
		# 		c_init_stack_count -= 1
		# 		cd_count += 1
		# 	# d
		# 	else:
		# 		d_init_stack_count += 1
		# elif f_event == 'M':
		# 	# cdm
		# 	if cd_count > 0:
		# 		cd_count -= 1
		# 		cdm += 1
		# 		hit += 1
		# 		malstring += 'CDM'
		# 		ransom_score += cdm_weight
		# 	# dcm
		# 	elif dc_count > 0:
		# 		dc_count -= 1
		# 		dcm += 1
		# 		hit += 1
		# 		malstring += 'DCM'
		# 		ransom_score += dcm_weight
		# 	# mum
		# 	elif mu_count > 0:
		# 		mu_count -= 1
		# 		mum += 1
		# 		hit += 1
		# 		malstring += 'MUM'
		# 		ransom_score += mum_weight
		# 	# mmm
		# 	elif mm_count > 0:
		# 		mm_count -= 1
		# 		mmm += 1
		# 		hit += 1
		# 		malstring += 'MMM'
		# 		ransom_score += mmm_weight
		# 	# mm
		# 	elif m_init_stack_count > 0:
		# 		m_init_stack_count -= 1
		# 		mm_count += 1
		# 	# cm
		# 	elif c_init_stack_count > 0:
		# 		c_init_stack_count -= 1
		# 		cm_count += 1		
		# 	# m
		# 	else:
		# 		m_init_stack_count += 1
		# elif f_event == 'U':
		# 	# cdu
		# 	if cd_count > 0:
		# 		cd_count -= 1
		# 		cdu += 1
		# 		hit += 1
		# 		malstring += 'CDU'
		# 		ransom_score += cdu_weight
		# 	# dcu
		# 	elif dc_count > 0:
		# 		dc_count -= 1
		# 		dcu += 1
		# 		hit += 1
		# 		malstring += 'DCU'
		# 		ransom_score += dcu_weight
		# 	# cu
		# 	elif c_init_stack_count > 0:
		# 		c_init_stack_count -= 1
		# 		cu_count += 1
		# 	# mu
		# 	elif m_init_stack_count > 0:
		# 		m_init_stack_count -= 1
		# 		mu_count += 1
		# 	# u (no)
		# 	else:
		# 		pass
		# else:
		# 	break
		
		if count >= logfile_record_length:
			count = 0
			break
		else:
			continue

	# rank_1_gram_dict = sort_dict(rank_1_gram_dict)
	# rank_2_gram_dict = sort_dict(rank_2_gram_dict)
	# rank_3_gram_dict = sort_dict(rank_3_gram_dict)
	# rank_4_gram_dict = sort_dict(rank_4_gram_dict)

	# for key, value in rank_1_gram_dict.items():
	# 	if key in total_rank_1_gram_dict:
	# 		total_rank_1_gram_dict[key] += value
	# 	else:
	# 		total_rank_1_gram_dict[key] = value
	# for key, value in rank_2_gram_dict.items():
	# 	if key in total_rank_2_gram_dict:
	# 		total_rank_2_gram_dict[key] += value
	# 	else:
	# 		total_rank_2_gram_dict[key] = value
	# for key, value in rank_3_gram_dict.items():
	# 	if key in total_rank_3_gram_dict:
	# 		total_rank_3_gram_dict[key] += value
	# 	else:
	# 		total_rank_3_gram_dict[key] = value
	# for key, value in rank_4_gram_dict.items():
	# 	if key in total_rank_4_gram_dict:
	# 		total_rank_4_gram_dict[key] += value
	# 	else:
	# 		total_rank_4_gram_dict[key] = value

	cdm_list.append(cdm)
	cmd_list.append(cmd)
	dcm_list.append(dcm)
	mmm_list.append(mmm)
	cdu_list.append(cdu)
	cud_list.append(cud)
	dcu_list.append(dcu)
	mum_list.append(mum)

	print "***********************"
	# print rank_1_gram_dict
	# print rank_2_gram_dict
	# print rank_3_gram_dict
	# print rank_4_gram_dict
	print "hit count : " + str(hit)
	print "cdm : " + str(cdm)
	print "cmd : " + str(cmd)
	print "dcm : " + str(dcm)
	print "mmm : " + str(mmm)
	print "cdu : " + str(cdu)
	print "cud : " + str(cud)
	print "dcu : " + str(dcu)
	print "mum : " + str(mum)
	print "malstring : " + malstring
	print "***********************"


	ransom_score_list.append(ransom_score)
	file_name_list.append(file_name)

	malstring += "\n\nransom score : " + str(ransom_score) + "\ncdm : " + str(cdm) + "\ncmd : " + str(cmd) + "\ndcm : " + str(dcm) + "\nmmm : " + str(mmm) + "\ncdu : " + str(cdu) + "\ncud : " + str(cud) + "\ndcu : " + str(dcu) + "\nmum : " + str(mum)
	# malstring = ""
	return malstring


def sort_dict(rank_dict):
	return sorted(rank_dict.items(), key=operator.itemgetter(1), reverse=True)


# 보정값 * 2
def parse_redo_create_data(redo_data, offset):
	redo_file = cStringIO.StringIO(redo_data)
	# print "redo_file : " + str(redo_file.read())
	# redo_file.seek(0)
	redo_offset = 0
	# print "offset : " + hex(offset)
	redo_file.seek(0, os.SEEK_END)
	redo_file_size = 0
	redo_file_size = redo_file.tell()
	redo_file.seek(0)

	# skip mft entry header (FILE...)
	redo_file.seek(56 * 2)
	redo_offset += 56 * 2

	### redo_file raw data
	# print "redo_file"
	# for i in range(0, redo_file_size):
	# 	print redo_file.read(1),
	# 	if i % 32 == 31:
	# 		print ''
	# 	elif i % 8 == 7:
	# 		print ' ',

	# print "redo_file : " + str(redo_file.read())
	redo_file.seek(redo_offset)

	while True:
		# print "redo_offset, redo_file_size"
		# print str(redo_offset) + " , " + str(redo_file_size)
		if redo_offset >= redo_file_size:
			redo_file.close()
			break

		# print "offset : " + hex(offset)

		attr_type = redo_file.read(8)
		# print "attr_type : " + attr_type

		length_1 = redo_file.read(1 * 2)
		length_2 = redo_file.read(1 * 2)
		length_3 = redo_file.read(1 * 2)
		length_4 = redo_file.read(1 * 2)

		length = length_4 + length_3 + length_2 + length_1

		if length == '':
			redo_file.close()
			break

		# print "length : " + length + " , " + str(int(length, base=16))
		# print "redo_offset : " + str(redo_offset)
		# non resident flag
		nr_flag = redo_file.read(1 * 2)

		# redo_offset += 9 * 2

		# resident
		if nr_flag == '00':
			name_length = redo_file.read(1 * 2)

			if name_length == '':
				redo_file.close()
				break

			# print "name_length : " + name_length + " , " + str(int(name_length, base=16))

			# offset to the name, flags, attr id
			redo_file.seek((2 + 2 + 2) * 2, 1)

			attr_length_1 = redo_file.read(1 * 2)
			attr_length_2 = redo_file.read(1 * 2)
			attr_length_3 = redo_file.read(1 * 2)
			attr_length_4 = redo_file.read(1 * 2)

			attr_length = attr_length_4 + attr_length_3 + attr_length_2 + attr_length_1

			if attr_length == '':
				redo_file.close()
				break

			# print "attr_length : " + attr_length + " , " + str(int(attr_length, base=16))

			if int(attr_length, base=16) > 4096 - 122:
				redo_file.close()
				break

			# skip all
			# offset to the attr, indexed flag, padding
			redo_file.seek((2 + 1 + 1) * 2, 1)
			redo_file.seek(int(name_length, base=16) * 2, 1)

			# redo_offset += (19 + int(name_length, base=16)) * 2

			if attr_type == '10000000':
				ctime = redo_file.read(8 * 2) # creation
				atime = redo_file.read(8 * 2) # altered
				mtime = redo_file.read(8 * 2) # mft changed
				rtime = redo_file.read(8 * 2) # read

				redo_offset += int(length, base=16) * 2
				# print "redo_offset : " + str(redo_offset)
				# to the next attr
				redo_file.seek(redo_offset)
				continue

			elif attr_type == '30000000':
				# skip all haha
				redo_file.seek(56 * 2, 1)
				dir_flag = redo_file.read(4 * 2)
				redo_file.seek(4 * 2, 1)
				file_name_len = redo_file.read(1 * 2)
				if file_name_len == '':
					redo_file.close()
					break
				# print "file_name_len : " + file_name_len + " , " + str(int(file_name_len, base=16))
				# file name namespace?
				redo_file.seek(1 * 2, 1)

				file_name = ""
				# print "file name area : " + redo_file.read(int(file_name_len, base=16) * 2)
				# redo_file.seek(-int(file_name_len, base=16), 1)
				for i in range(0, int(file_name_len, base=16) * 2):
					file_name = file_name + redo_file.read(1 * 2)

				if dir_flag == '00000010':
					pass
					# print "created directory name : " + file_name.decode('hex')	
				else:
					pass
					# print "created file name : " + file_name.decode('hex')
				redo_offset += int(length, base=16) * 2
				# print "redo_offset : " + str(redo_offset)
				redo_file.seek(redo_offset)
				continue

			elif attr_type == '80000000':
				data = ""
				for i in range(0, int(attr_length, base=16)):
					data = data + redo_file.read(1 * 2)
				# print "redo_data : " + data
				redo_offset += int(length, base=16) * 2
				# print "redo_offset : " + str(redo_offset)
				redo_file.seek(redo_offset)
				continue

			else:
				# end
				# print "end of redo attr"
				if int(length, base=16) == 0 or int(length, base=16) > 4096:
					redo_file.close()
					break
				redo_offset += int(length, base=16) * 2
				redo_file.seek(redo_offset)
				continue
				# break

		# non resident
		else:
			# print "nr is not 00"
			if int(length, base=16) == 0 or int(length, base=16) > 4096:
				redo_file.close()
				break

			redo_offset += int(length, base=16) * 2
			redo_file.seek(redo_offset)
			continue


def parse_undo_delete_data(undo_data, offset):
	undo_file = cStringIO.StringIO(undo_data)
	# print "undo_file : " + str(undo_file.read())
	# undo_file.seek(0)
	undo_offset = 0
	# print "offset : " + hex(offset)
	undo_file.seek(0, os.SEEK_END)
	undo_file_size = 0
	undo_file_size = undo_file.tell()
	undo_file.seek(0)

	undo_file.seek(72 * 2 , 1)
	file_name_length = int(undo_file.read(1 * 2), base=16)
	undo_file.seek(9 * 2, 1)
	file_name = ""
	for i in range (0, file_name_length):
		file_name = file_name + undo_file.read(1 * 2)
	file_name = file_name.decode('hex')
	# print "deleted file name : " + file_name


def parse_redo_update_data(redo_data, offset):
	pass


def parse_undo_update_data(undo_data, offset):
	pass


def parse_redo_prev_rename_data(redo_data):
	# print "prev redo data : " + redo_data
	if redo_data == '':
		# print "prev file name : "
		return
	redo_file = cStringIO.StringIO(redo_data)
	redo_offset = 0
	# print "offset : " + hex(offset)
	redo_file.seek(0, os.SEEK_END)
	redo_file_size = 0
	redo_file_size = redo_file.tell()
	redo_file.seek(0)

	redo_file.seek(88 * 2, 1)
	file_name_length = redo_file.read(1 * 2)
	if file_name_length == '':
		# print "crn file name : unknown"
		return
	file_name_length = int(file_name_length, base=16)
	redo_file.seek(1 * 2, 1)
	file_name = ''
	for i in range(0, file_name_length * 2):
		file_name = file_name + redo_file.read(1 * 2)
	file_name = file_name.decode('hex')
	# print "prev file name : " + file_name


def parse_redo_crn_rename_data(redo_data):
	# print "crn redo data : " + redo_data
	if redo_data == '':
		# print "crn file name : "
		return
	redo_file = cStringIO.StringIO(redo_data)
	redo_offset = 0
	# print "offset : " + hex(offset)
	redo_file.seek(0, os.SEEK_END)
	redo_file_size = 0
	redo_file_size = redo_file.tell()
	redo_file.seek(0)

	redo_file.seek(88 * 2, 1)
	file_name_length = redo_file.read(1 * 2)
	if file_name_length == '':
		# print "crn file name : unknown"
		return
	file_name_length = int(file_name_length, base=16)
	redo_file.seek(1 * 2, 1)
	file_name = ''
	for i in range(0, file_name_length * 2):
		file_name = file_name + redo_file.read(1 * 2)
	file_name = file_name.decode('hex')
	# print "crn file name : " + file_name


def main():
	global mal_count_dict, ransom_score_list

	parse_logfile()
	numeric_result = open('result/numeric_result.txt', 'w')
	numeric_result.write("cdm mean : %d\t, cdm var : %d\t, cdm dev : %d\t\n" % (np.mean(cdm_list), np.var(cdm_list), np.std(cdm_list)))
	numeric_result.write("cud mean : %d\t, cud var : %d\t, cud dev : %d\t\n" % (np.mean(cud_list), np.var(cud_list), np.std(cud_list)))
	numeric_result.write("dcm mean : %d\t, dcm var : %d\t, dcm dev : %d\t\n" % (np.mean(dcm_list), np.var(dcm_list), np.std(dcm_list)))
	numeric_result.write("dcu mean : %d\t, dcu var : %d\t, dcu dev : %d\t\n" % (np.mean(dcu_list), np.var(dcu_list), np.std(dcu_list)))
	numeric_result.write("cmd mean : %d\t, cmd var : %d\t, cmd dev : %d\t\n" % (np.mean(cmd_list), np.var(cmd_list), np.std(cmd_list)))
	numeric_result.write("cdu mean : %d\t, cdu var : %d\t, cdu dev : %d\t\n" % (np.mean(cdu_list), np.var(cdu_list), np.std(cdu_list)))
	numeric_result.write("mmm mean : %d\t, mmm var : %d\t, mmm dev : %d\t\n" % (np.mean(mmm_list), np.var(mmm_list), np.std(mmm_list)))
	numeric_result.write("mum mean : %d\t, mum var : %d\t, mum dev : %d\t\n" % (np.mean(mum_list), np.var(mum_list), np.std(mum_list)))

	ransom_mean = np.mean(ransom_score_list)
	ransom_std = np.std(ransom_score_list)

	for i in range(0, len(file_name_list)):
		numeric_result.write(str(file_name_list[i]) + "," + str((ransom_score_list[i] - ransom_mean)/ransom_std) + "\n")

	numeric_result.close()
	# print_logfile_data(total_data_list)
	# mal_result = open('result/mal_total_result.txt', 'w')
	# mal_result = open('result/ben_total_result.txt', 'w')
	# avg_value = 0
	# for key, value in mal_count_dict.iteritems():
	# 	mal_result.write(key + " : %d\n" % value)
	# 	avg_value += value

	# avg_value = avg_value / len(mal_count_dict)
	# mal_result.write("\n\naverage count : " + str(avg_value))
	# mal_result.close()
	
if __name__ == '__main__':
	main()


	# cdm_count = 0
	# cmd_count = 0
	# dcm_count = 0
	# mmm_count = 0                              
	
	# c_count = 0
	# d_count = 0
	# m_count = 0

	# mal_count = 0

	# while True:
	# 	f_event = record_string.read(1)
	# 	count += 1

	# 	if f_event == 'C':
			
	# 		if c_count > 0 and d_count > 0:
	# 			d_count -= 1
	# 			m_count -= 1
	# 			mal_count += 1
	# 		else:
	# 			c_count += 1

	# 	elif f_event == 'D':

	# 		if c_count > 0 and m_count > 0:
	# 			c_count -= 1
	# 			m_count -= 1
	# 			mal_count += 1
	# 		else:
	# 			d_count += 1

	# 	elif f_event == 'M':

	# 		if c_count > 0 and d_count > 0:
	# 			c_count -= 1
	# 			d_count -= 1
	# 			mal_count += 1
	# 		else:
	# 			m_count += 1

	# 			if m_count == 3:
	# 				m_count = 0
	# 				mal_count += 1
	# 			else:
	# 				pass

	# 	else:
	# 		# something wrong
	# 		break

	# 	if count >= logfile_record_length:
	# 		count = 0
	# 		break
	# 	else:
	# 		continue