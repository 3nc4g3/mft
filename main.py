#-*- coding: utf-8 -*-
# Author : Kali-KM
# Blog : kali-km.tistory.com
class Path_Parsing:

	def __init__(self):
		self.seq=0
		self.arr=[]
		self.name_seq={}
		self.parent_seq={}
		self.full_path={}
	
		try:
			self.f=open('Output_$MFT','rb')
		except IOError:
			print "[-] Error : MFT File doesn't exist."
		

	def Return_DirPath(self):	# Return [0] is a sequence Number, [1] is a directory path
		self.Read_MFT()
		self.Build_path()
		
		return self.full_path


	# k = Path_Parsing()
	# a = k.Return_DirPath()
	# self.full_path.keys() : Sequence Number
	# self.full_path.values()	: Directory Path
	def Build_path(self):
		for i in self.arr:
			path=''
			init=i
			
			while True:
				try:
					tmp_path=str(self.name_seq[init])
					tmp_path='\\'+tmp_path
					path=tmp_path+path
					p_seq=self.parent_seq.get(init)
					init=p_seq

					if p_seq == 5:
						break

				except KeyError:break

			# print path
			self.full_path[i]=path

		print len(self.full_path)
		return


	def Read_MFT(self):
		while True:
			self.buf = bytearray(self.f.read(0x400))
			if len(self.buf) != 0x400:
				print '[+] Success Read File.'
				break

			if LtoI(self.buf[0x16:0x18]) == 0x3:
				tmp=self.Dir_name_seq()
				# seq_num=LtoI(self.buf[0x10:0x12])
				self.arr.append(self.seq)
				self.name_seq[self.seq]=tmp[0]
				self.parent_seq[self.seq]=tmp[1]
			else:
				pass
			self.seq+=1
		return


	def Dir_name_seq(self):
		attr_off=LtoI(self.buf[0x14:0x16])
		attr_buf=self.buf[attr_off:]
		dir_name=None
		dir_pseq=None

		while True:
			attr_type=LtoI(attr_buf[0x00:0x04])
			attr_size=LtoI(attr_buf[0x04:0x08])
			if attr_size==0: break
			if attr_type==0x00 | attr_type==0xFFFF: break

			if attr_type==0x30:
				dir_pseq=LtoI(attr_buf[0x18:0x1b])
				dir_name_len=attr_buf[0x58]*2
				dir_name=Remove_uni_null(attr_buf[0x5a:0x5a+dir_name_len])

			attr_buf=attr_buf[attr_size:]

		return dir_name,dir_pseq
	# end class


def Remove_uni_null(uni_str):
	tmp=[]
	count=0

	while True:
		if count==len(uni_str):break

		if uni_str[count+1] !=0:
			han=Read_Han(uni_str[count:count+2])
			tmp.append(han)

		else:
			tmp.append(chr(uni_str[count]))
		count+=2

	return ''.join(tmp)


def Read_Han(buf):
	return buf.decode('utf-16').encode('mbcs')


def LtoI(buf):	# Little Endian To Integer
	val =0

	for i in range(0, len(buf)):
		multi = 1

		for j in range(0,i):
			multi *= 256

		val += buf[i] * multi

	return val