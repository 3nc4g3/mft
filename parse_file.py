#!/usr/bin/env python
# coding:utf-8

from argparse import ArgumentParser

class FileParser:
    index = ['record length', 
             'major version', 
             'minor version', 
             'usn', 
             'timestamp', 
             'reason', 
             'source info', 
             'security id', 
             'file attributes', 
             'file name length', 
             'file name offset', 
             'file name', 
             'file reference mft entry', 
             'file reference sequence number', 
             'parent file reference mft entry', 
             'parent file reference sequence number']
    record_len = None
    file_list_dict = None
    temp_file_list = None
    file_operation_dict = None
    def __init__(self, usnjrnl_path):
        self.temp_file_list = []
        with open(usnjrnl_path, 'r') as f:
            f.readline()
            self.temp_file_list = f.readlines()
            self.record_len = len(self.temp_file_list)
            self.file_list_dict = [{} for _ in range(0, self.record_len)]
        for i in range(0, self.record_len):
            temp = self.temp_file_list[i].split(',')
            for j in range(0, len(self.index)):
                self.file_list_dict[i][self.index[j]] = temp[j]

    """
    0x00000001: 'DATA_OVERWRITE',
    0x00000002: 'DATA_EXTEND',
    0x00000004: 'DATA_TRUNCATION',
    0x00000010: 'NAMED_DATA_OVERWRITE',
    0x00000020: 'NAMED_DATA_EXTEND',
    0x00000040: 'NAMED_DATA_TRUNCATION',
    0x00000100: 'FILE_CREATE',
    0x00000200: 'FILE_DELETE',
    0x00000400: 'EA_CHANGE',
    0x00000800: 'SECURITY_CHANGE',
    0x00001000: 'RENAME_OLD_NAME',
    0x00002000: 'RENAME_NEW_NAME',
    0x00004000: 'INDEXABLE_CHANGE',
    0x00008000: 'BASIC_INFO_CHANGE',
    0x00010000: 'HARD_LINK_CHANGE',
    0x00020000: 'COMPRESSION_CHANGE',
    0x00040000: 'ENCRYPTION_CHANGE',
    0x00080000: 'OBJECT_ID_CHANGE',
    0x00100000: 'REPARSE_POINT_CHANGE',
    0x00200000: 'STREAM_CHANGE',
    0x80000000: 'CLOSE'
    """

    def make_file_operation_dict(self):

        self.file_operation_dict = {str:str}

        for i in range(0, self.record_len):
            file_name = self.file_list_dict[i]['file name']
            self.file_operation_dict[file_name] = ''


        for i in range(0, self.record_len):
            file_name = self.file_list_dict[i]['file name']
            operation = self.file_list_dict[i]['reason']

            if 'DATA' in operation or 'ENCRYPTION' in operation:
                self.file_operation_dict[file_name] += 'U'
            elif 'CREATE' in operation:
                self.file_operation_dict[file_name] += 'C'
            elif 'DELETE' in operation:
                self.file_operation_dict[file_name] += 'D'
            elif 'RENAME_OLD' in operation:
                #여기서 링크해줄 필요성이 존재한다.
                self.file_operation_dict[file_name] += 'M'

            if 'RENAME' in operation:
                if 'OLD' in operation:

                elif 'NEW' in operation:


    def export_file_operation_dict(self):
        with open('./test.txt', 'w+') as f:
            for file_name in self.file_operation_dict.keys():
                f.write('%s : %s\n'%(file_name, self.file_operation_dict[file_name]))
            # self.file_opeartion_dict[self.file_list_dict['file name']]





if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-f',
                        help='extracted $UsnJrnl file',
                        dest='usnfile'
                        )
    args = parser.parse_args()
    fp = FileParser(args.usnfile)
