# MFT Acquisition

import wx
import win32api
import ctypes
import os, sys
import struct
import binascii
from StringIO import StringIO
import pytsk3
import time
import math

#constant size values
longlongsize=ctypes.sizeof(ctypes.c_longlong)
bytesize=ctypes.sizeof(ctypes.c_byte)
wordsize=2
dwordsize=4

s_time = time.time()

class Extract_NTFS:

	def __init__(self, selectedDriveLetter):
		self.volume='\\\\.\\'+selectedDriveLetter[0] + ":"
		self.img=pytsk3.Img_Info(self.volume) 
		self.fs=pytsk3.FS_Info(self.img)

	def Extract(self, filename): 
		f=self.fs.open(filename)
		barr = bytearray()

		offset=0 
		size=f.info.meta.size 

		while offset<size: 
			available_to_read=min(1024*1024,size-offset) 
			buf = f.read_random(0,available_to_read) 

			if not buf: 
				break 
			barr.extend(buf)
			offset+=len(buf) 

		return barr

class Frame(wx.Frame):
	def __init__(self, driveList):
		wx.Frame.__init__(self, None, -1, "MFT parser v1.0", 
			size = (800, 600))

		self.CreateStatusBar()
		self.panel = wx.Panel(self)
		self.selectedDriveLetter = ""

		driveInfoText = wx.StaticText(self.panel, -1, "Select Drive : ")
		
		v_mainSizer = wx.BoxSizer(wx.VERTICAL)
		h_driveSizer = wx.BoxSizer(wx.HORIZONTAL)

		############# LINE 1 (Drive Selection)
		self.driveChoice = wx.Choice(self.panel, choices=driveList)
		self.driveChoice.Bind(wx.EVT_CHOICE, self.onChoiceDrive)
		driveButton = wx.Button(self.panel, label="Scan MFT")
		driveButton.Bind(wx.EVT_BUTTON, self.onClickDriveButton)

		h_driveSizer.Add(driveInfoText, 1, wx.ALL, 10) 
		h_driveSizer.Add(self.driveChoice, 1, wx.ALL, 10)
		h_driveSizer.Add(driveButton, 1, wx.ALL, 10)
		
		v_mainSizer.Add(h_driveSizer, flag = wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, border=5)
		
		############# LINE 2 (Showing full hex data of MFT)
		fullHexInfoText = wx.StaticText(self.panel, -1, "Hex data of MFT")
		self.fullHexCtrl = wx.TextCtrl(self.panel, -1, pos=(10, 10), size=(600, 200), style = wx.TE_MULTILINE)

		v_mainSizer.Add(fullHexInfoText, flag = wx.LEFT, border = 15)
		v_mainSizer.Add(self.fullHexCtrl, flag = wx.EXPAND | wx.LEFT | wx.RIGHT, border=15)


		############# LINE 3 (Showing interesting points)

		self.panel.SetSizer(v_mainSizer)
		self.Centre()

	def onChoiceDrive(self, e):
		self.selectedDriveLetter = self.driveChoice.GetStringSelection()

	def onClickDriveButton(self, e):
		
		if self.selectedDriveLetter == "":
			dlg = wx.MessageDialog(self.panel, "Select Drive", "Error", wx.OK | wx.ICON_WARNING)
			dlg.ShowModal()
			dlg.Destroy()
			return

		# Check the time for extraction & display of LogFile
		s_time = time.time()
		Ext = Extract_NTFS(self.selectedDriveLetter)
		
		# res = raw hex data of logfile (bytearray)
		res = Ext.Extract('/$LogFile')

		logfile_ext_time = time.time() - s_time
		print("--- [Logfile Extraction] Elapsed %s sec ---" % logfile_ext_time)

		prcsLogfile(self.fullHexCtrl, res)

def main():
	# initialize for GUI
	app = wx.App(redirect=True, filename="tmp/wxlog.txt")
	drives = findPossibleDrives()
	frame = Frame(drives)
	frame.Show()
	app.MainLoop()

def findPossibleDrives():
	drives = win32api.GetLogicalDriveStrings()
	drives = drives.split("\000")[:-1]
	return drives

def prcsLogfile(textbox, res):
	# Input : bytestring of logfile

	# Calculate entropy of logfile
	#res_string = binascii.hexlify(res)

	logfile_dip_time = time.time() - s_time
	print("--- [Logfile Entropy Calculation] Elapsed %s sec ---" % logfile_dip_time)

	lf_ent = calc_entropy(res)
	print "Logfile entropy : " + str(lf_ent)
	return
	
# Thanks to Jeff Bryner (pyMFTGrabber)
#utility functions for printing data as hexdumps
def hexbytes(xs, group_size=1, byte_separator=' ', group_separator=' '):
	def ordc(c):
		return ord(c) if isinstance(c,str) else c
	
	if len(xs) <= group_size:
		s = byte_separator.join('%02X' % (ordc(x)) for x in xs)
	else:
		r = len(xs) % group_size
		s = group_separator.join(
			[byte_separator.join('%02X' % (ordc(x)) for x in group) for group in zip(*[iter(xs)]*group_size)]
		)
		if r > 0:
			s += group_separator + byte_separator.join(['%02X' % (ordc(x)) for x in xs[-r:]])
	return s.lower()


# Thanks to Jeff Bryner (pyMFTGrabber)
def hexprint(xs):
	def chrc(c):
		return c if isinstance(c,str) else chr(c)
	
	def ordc(c):
		return ord(c) if isinstance(c,str) else c
	
	def isprint(c):
		return ordc(c) in range(32,127) if isinstance(c,str) else c > 31
	
	return ''.join([chrc(x) if isprint(x) else '.' for x in xs])

# Thanks to Jeff Bryner (pyMFTGrabber)
def hexdump(xs, group_size=4, byte_separator=' ', group_separator='-', printable_separator='  ', address=0, address_format='%04X', line_size=16):
	if address is None:
		s = hexbytes(xs, group_size, byte_separator, group_separator)
		if printable_separator:
			s += printable_separator + hexprint(xs)
	else:
		r = len(xs) % line_size
		s = ''
		bytes_len = 0
		for offset in range(0, len(xs)-r, line_size):
			chunk = xs[offset:offset+line_size]
			bytes = hexbytes(chunk, group_size, byte_separator, group_separator)
			s += (address_format + ': %s%s\n') % (address + offset, bytes, printable_separator + hexprint(chunk) if printable_separator else '')
			bytes_len = len(bytes)
		
		if r > 0:
			offset = len(xs)-r
			chunk = xs[offset:offset+r]
			bytes = hexbytes(chunk, group_size, byte_separator, group_separator)
			bytes = bytes + ' '*(bytes_len - len(bytes))
			s += (address_format + ': %s%s\n') % (address + offset, bytes, printable_separator + hexprint(chunk) if printable_separator else '')
	
	return s

def range_bytes (): return range(256)
def range_printable(): return (ord(c) for c in string.printable)

def calc_entropy(data, iterator=range_bytes):
    if not data:
        return 0
    entropy = 0
    for x in iterator():
        p_x = float(data.count(chr(x)))/len(data)
        if p_x > 0:
            entropy += - p_x*math.log(p_x, 2)
    return entropy

if __name__ == "__main__":
	main()