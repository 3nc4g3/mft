0. file event
 1) creation
 2) deletion
 3) move
 4) update(optional)

1. define unique pattern

 1) rules
 	i) Based on single file behavior
 	ii) C and D exist in pair only
 	iii) M (and U) cannot be located right after D
 	iv) 1,2 gram is too short (too general) and 4 > grams are subset of 3-grams.

 2) cases
 	3-grams) CMD, CDM, DCM (CUD, CDU, DCU)

 3) other cases...

 	C 	D 	M
 ------------------
 1	O 	O 
 2 	 	 	O
 3 	O 	O 	O


2. multi thread

 1) Can we use a stack?

 	C 	D 	M
 ------------------
 1	O 	O 
 2 	 	 	O
 3 	O 	O 	O

 - push C, push D
 - push M
 - push D, push C, push M
 - ...

 2) Information of each events (rename results...)
 	i) file name (without extension)
 	ii) LCN (logical cluster number, update or move)
 	iii) time...?


3. Reduce false-positive
 1) weight average (each gram has a unique weight), weightsum average
 2) take a threshold (benign / malicious)

4. and so on... (comparison, limitation, discussion, evaluation...)